FROM alpine:edge

ADD https://github.com/Motion-Project/motion/archive/release-4.2.tar.gz /tmp/motion.tar.gz
ADD ./.motion /root/.motion
ADD ./websocat_amd64-linux-static /usr/bin/websocat

RUN apk update && \
    apk add ffmpeg-libs libjpeg libmicrohttpd && \
    apk add --no-cache --virtual .build-deps \
      build-base automake autoconf ffmpeg-dev jpeg-dev libmicrohttpd-dev && \
    cd /tmp && \
    tar -zxvf /tmp/motion.tar.gz && \
    cd /tmp/motion-release-4.2 && \
    autoreconf -i && ./configure && make -j8 && make install && \
    apk del .build-deps && \
    rm -rf /tmp/motion* /var/cache/apk/*


ENTRYPOINT ["motion"]
